#include <vector>
#include <stdlib.h>
#include <iostream>
#include "Sessions.h"

using namespace std;

static char charset[] = "abcdeghijklmnpqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
#define CHARSET_SIZE (sizeof(charset) / sizeof(char))

namespace Mongoose
{
Sessions::Sessions(string key_)
	: m_key(key_)
{
}

Sessions::~Sessions()
{
	for (auto it : m_sessions) {
		delete (it).second;
	}
}

string Sessions::getId(Request &request, Response &response)
{
	if (request.hasCookie(m_key)) {
		return request.getCookie(m_key);
	} else {
		ostringstream newCookie;
		int i;

		for (i = 0; i < 30; ++i) {
			newCookie << charset[rand() % CHARSET_SIZE];
		}

		response.setCookie(m_key, newCookie.str());

		return newCookie.str();
	}
}

Session &Sessions::get(Request &request, Response &response)
{
	string id = getId(request, response);
	Session *session = nullptr;

	std::lock_guard<std::mutex> guard(m_mutex);
	if (m_sessions.find(id) != m_sessions.end()) {
		session = m_sessions[id];
	} else {
		session = new Session();
		m_sessions[id] = session;
	}
	return *session;
}

void Sessions::garbageCollect(int oldAge)
{
	vector<string> deleteList;

	std::lock_guard<std::mutex> guard(m_mutex);
	for (auto it : m_sessions) {
		string name = (it).first;
		Session *session = (it).second;
		if (session->getAge() > oldAge) {
			delete session;
			deleteList.push_back(name);
		}
	}

	for (auto vit : deleteList) {
		m_sessions.erase(vit);
	}
}
}
