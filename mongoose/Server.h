#ifndef _MONGOOSE_SERVER_H
#define _MONGOOSE_SERVER_H

#include <mutex>
#include <vector>
#include <iostream>

#include "mongoose.h"
#include "Request.h"
#include "Response.h"
#include "Controller.h"
#ifndef NO_WEBSOCKET
#include "WebSocket.h"
#include "WebSockets.h"
#endif
#include "Sessions.h"

/**
 * Wrapper for the Mongoose server
 */
namespace Mongoose
{
class Server {
public:
	/**
	 * Constructs the server
	 *
	 * @param int the number of the port to listen to
	 * @param string documentRoot the root that should be used for static files
	 */
	Server(int port = 80, const char *documentRoot = "www");
	virtual ~Server();

	/**
	 * Runs the Mongoose server
	 * if enableThread: All the processing is run in an internal thread.
	 * if false: the user has to cyclically all process.
	 */
	void start(bool enableThread = true);

	/**
	 * Stops the Mongoose server
	 */
	void stop();

	/**
	 * Register a new controller on the server
	 *
	 * @param Controller* a pointer to a controller
	 */
	void registerController(Controller *);

	/**
	 * Internally used to process a request
	 *
	 * @param struct mg_connection* the mongoose connection
	 */
	int _handleRequest(struct mg_connection *conn);

	/**
	 * Internally used to process a file upload
	 *
	 * @param struct mg_connection* the mongoose conneciton
	 * @param const char * the name of the uploaded file
	 */
	void _upload(struct mg_connection *conn, const char *fileName);

	/**
	 * Handles a web socket connection
	 *
	 * @param struct mg_connection* the mongoose connection with the client
	 */
	void _webSocketReady(struct mg_connection *conn);

	/**
	 * Handles web sockets data
	 *
	 * @param struct mg_connection* the mongoose connection
	 * @param int flags the data flags
	 * @param char* the data
	 * @param size the data size
	 *
	 * @return int if we have to keep the connection opened
	 */
	int _webSocketData(struct mg_connection *conn, std::string data);

	/**
	 * remove a websocket. Socket is identified by connection
	 *
	 * @param conn
	 */
	void _removeWebSocket(struct mg_connection *conn);

	/**
	 * Process the request by controllers
	 *
	 * @param Request the request
	 *
	 * @return Response the response if one of the controllers can handle it,
	 *         nullptr else
	 */
	Response *handleRequest(Request &request);

	/**
	 * Sets a mongoose extra option
	 *
	 * @param string the name of the option
	 * @param string the value of the option
	 */
	void setOption(std::string key, std::string value);

#ifndef NO_WEBSOCKET
	/**
	 * Returns the WebSockets container
	 *
	 * @return WebSockets the web sockets container
	 */
	WebSockets &getWebSockets();
#endif

	/**
	 * Print statistics
	 */
	void printStats();

	/**
	 * process. The main mongoose polling.
	 * @param millisecs
	 */
	void process(int millisecs);

	/**
	 * Polls the server.
	 * Do not use this.
	 * It will be called if the server is started as threading type.
	 */
	void poll();

	/**
	 * Does the server handles url?
	 */
	bool handles(std::string method, std::string url);

protected:
	volatile bool m_stopped{ false };
	volatile bool m_destroyed{ false };
	Sessions m_sessions;
	std::mutex m_mutex;
	std::map<std::string, std::string> m_optionsMap;
	std::map<struct mg_connection *, Request *> m_currentRequests;
	struct mg_server *m_server{ nullptr };

#ifndef NO_WEBSOCKET
	WebSockets m_websockets;
#endif

	std::vector<Controller *> m_controllers;
	// Statistics
	int m_requests{ 0 };
	int m_startTime{ 0 };
	bool m_threadEnabled{ false };
};
}

#endif
