#include "WebController.h"
#include "Session.h"

namespace Mongoose
{
WebController::WebController(int gcDivisor_)
	: Controller()
	, m_gcDivisor(gcDivisor_)
{
}

void WebController::preProcess(Request &request, Response &response)
{
	{
		std::lock_guard<std::mutex> guard(m_mutex);
		m_counter++;

		if (m_counter > m_gcDivisor) {
			m_counter = 0;
			m_sessions->garbageCollect();
		}
	}
	Session &session = m_sessions->get(request, response);
	session.ping();
	response.setHeader("Content-Type", "text/html");
}
}
