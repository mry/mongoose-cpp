#ifndef _MSC_VER
#include <unistd.h>
#else
#include <time.h>
#include <sys/types.h>
#include <sys/timeb.h>
#endif
#include <string>
#include <string.h>
#include <iostream>
#include "Server.h"
#include "Utils.h"

using namespace std;
using namespace Mongoose;

static int getTime()
{
#ifdef _MSC_VER
	time_t ltime;
	time(&ltime);
	return ltime;
#else
	return time(nullptr);
#endif
}

static int iterate_callback(struct mg_connection *connection, enum mg_event);

static int do_i_handle(struct mg_connection *connection)
{
	Server *server = (Server *)connection->server_param;

	return server->handles(connection->request_method, connection->uri);
}

/**
 * The handlers below are written in C to do the binding of the C mongoose with
 * the C++ API
 */
static int event_handler(struct mg_connection *connection, enum mg_event ev)
{
	Server *server = (Server *)connection->server_param;

	if (server != nullptr) {
#ifndef NO_WEBSOCKET
		if (connection->is_websocket) {
			server->_webSocketReady(connection);
			if ((iterate_callback(connection, ev)) == MG_TRUE) {
				return MG_TRUE;
			}
		}
#endif
		switch (ev) {
		case MG_AUTH:
			return MG_TRUE;
		case MG_REQUEST:
			if (server->_handleRequest(connection)) {
				return MG_TRUE;
			}
			return MG_FALSE;

		case MG_CLOSE:
			if (connection->is_websocket) {
				server->_removeWebSocket(connection);
			}
			break;
		default:
			return MG_FALSE;
		}
	}

	return MG_TRUE;
}

#ifndef NO_WEBSOCKET
static int iterate_callback(struct mg_connection *connection, enum mg_event evt)
{
	if (evt == MG_REQUEST) {
		if (connection->is_websocket && connection->content_len) {
			// string out(connection->content, connection->content_len);
			// std::cout << "iterate_callback event:" << evt << " data:" << out << std::endl;

			Server *server = (Server *)connection->server_param;
			server->_webSocketData(connection, string(connection->content, connection->content_len));
		}
		return MG_TRUE;
	}
	return MG_FALSE;
}
#endif

static void *server_poll(void *param)
{
	Server *server = (Server *)param;
	server->poll();

	return nullptr;
}

namespace Mongoose
{
Server::Server(int port, const char *documentRoot)
#ifndef NO_WEBSOCKET
	: m_websockets(true)
#endif
{
	ostringstream portOss;
	portOss << port;
	m_optionsMap["listening_port"] = portOss.str();
	m_optionsMap["document_root"] = string(documentRoot);
	m_optionsMap["enable_keep_alive"] = "yes";
}

Server::~Server()
{
	stop();
}

void Server::start(bool enableThread)
{
	if (m_server == nullptr) {
		m_threadEnabled = enableThread;
#ifdef ENABLE_STATS
		m_requests = 0;
		m_startTime = getTime();
#endif
		m_server = mg_create_server(this, event_handler);
		for (auto it : m_optionsMap) {
			mg_set_option(m_server, (it).first.c_str(), (it).second.c_str());
		}

		m_stopped = false;
		if (enableThread) {
			mg_start_thread(server_poll, this);
		}
	} else {
		throw string("Server is already running");
	}
}

void Server::process(int millisecs)
{
	mg_poll_server(m_server, millisecs);
}

void Server::poll()
{
	while (!m_stopped) {
		process(200);
	}

	mg_destroy_server(&m_server);
	m_destroyed = true;
}

void Server::stop()
{
	m_stopped = true;
	if (m_threadEnabled) {
		while (!m_destroyed) {
			Utils::sleep(100);
		}
	} else {
		mg_destroy_server(&m_server);
		m_destroyed = true;
	}
}

void Server::registerController(Controller *controller)
{
	controller->setSessions(&m_sessions);
	controller->setServer(this);
	controller->setup();
	m_controllers.push_back(controller);
}

#ifndef NO_WEBSOCKET
void Server::_webSocketReady(struct mg_connection *conn)
{
	WebSocket *websocket = new WebSocket(conn);
	m_websockets.add(websocket);
	m_websockets.clean();
	for (auto it : m_controllers) {
		(it)->webSocketReady(websocket);
	}
}

void Server::_removeWebSocket(struct mg_connection *conn)
{
	m_websockets.remove(conn);
}

int Server::_webSocketData(struct mg_connection *conn, string data)
{
	WebSocket *websocket = m_websockets.getWebSocket(conn);

	if (websocket != nullptr) {
		websocket->appendData(data);

		string fullPacket = websocket->flushData();
		for (auto it : m_controllers) {
			(it)->webSocketData(websocket, fullPacket);
		}

		if (websocket->isClosed()) {
			m_websockets.remove(websocket);
			return 0;
		} else {
			return -1;
		}
	} else {
		return 0;
	}
}
#endif

int Server::_handleRequest(struct mg_connection *conn)
{
	Request request(conn);

	{
		std::lock_guard<std::mutex> guard(m_mutex);
		m_currentRequests[conn] = &request;
	}

	Response *response = handleRequest(request);

	{
		std::lock_guard<std::mutex> guard(m_mutex);
		m_currentRequests.erase(conn);
	}

	if (response == nullptr) {
		return 0;
	} else {
		request.writeResponse(response);
		delete response;
		return 1;
	}
}

bool Server::handles(string method, string url)
{
#ifndef NO_WEBSOCKET
	if (url == "/websocket") {
		return true;
	}
#endif

	for (auto it : m_controllers) {
		if (it->handles(method, url)) {
			return true;
		}
	}
	return false;
}

Response *Server::handleRequest(Request &request)
{
	Response *response;
	{
		std::lock_guard<std::mutex> guard(m_mutex);
		m_requests++;
	}
	for (auto it : m_controllers) {
		Controller *controller = it;
		response = controller->process(request);

		if (response != nullptr) {
			return response;
		}
	}

	return nullptr;
}

void Server::setOption(string key, string value)
{
	m_optionsMap[key] = value;
}

#ifndef NO_WEBSOCKET
WebSockets &Server::getWebSockets()
{
	return m_websockets;
}
#endif

void Server::printStats()
{
	int delta = getTime() - m_startTime;

	if (delta) {
		cout << "Requests: " << m_requests << ", Requests/s: " << (m_requests * 1.0 / delta) << endl;
	}
}
}
