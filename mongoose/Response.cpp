#include <sstream>
#include "Response.h"

using namespace std;

namespace Mongoose
{
Response::Response()
	: m_code(HTTP_OK)
	, m_headers()
{
}

void Response::setHeader(string key, string value)
{
	m_headers[key] = value;
}

bool Response::hasHeader(string key)
{
	return m_headers.find(key) != m_headers.end();
}

string Response::getData()
{
	string body = getBody();
	ostringstream data;

	data << "HTTP/1.0 " << m_code << "\r\n";

	if (!hasHeader("Content-Length")) {
		ostringstream length;
		length << body.size();
		setHeader("Content-Length", length.str());
	}

	map<string, string>::iterator it;
	for (it = m_headers.begin(); it != m_headers.end(); it++) {
		data << (*it).first << ": " << (*it).second << "\r\n";
	}

	data << "\r\n";

	data << body;

	return data.str();
}

void Response::setCookie(string key, string value)
{
	ostringstream definition;
	definition << key << "=" << value << "; path=/";

	setHeader("Set-cookie", definition.str());
}

void Response::setCode(int code_)
{
	m_code = code_;
}
}
