#include <time.h>
#include <iostream>
#include <string>
#include "Session.h"

using namespace std;

namespace Mongoose
{
Session::Session()
{
	ping();
}

void Session::ping()
{
	std::lock_guard<std::mutex> guard(m_mutex);
	m_date = time(nullptr);
}

void Session::setValue(string key, string value)
{
	std::lock_guard<std::mutex> guard(m_mutex);
	m_values[key] = value;
}

void Session::unsetValue(string key)
{
	std::lock_guard<std::mutex> guard(m_mutex);
	m_values.erase(key);
}

bool Session::hasValue(string key)
{
	return m_values.find(key) != m_values.end();
}

string Session::get(string key, string fallback)
{
	std::lock_guard<std::mutex> guard(m_mutex);
	if (hasValue(key)) {
		string value = m_values[key];
		return value;
	} else {
		return fallback;
	}
}

int Session::getAge()
{
	return time(nullptr) - m_date;
}
}
