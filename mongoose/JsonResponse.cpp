#include <sstream>
#include <jsoncpp/json/json.h>
#include "JsonResponse.h"

using namespace std;

namespace Mongoose
{
JsonResponse::JsonResponse()
	: m_humanReadable(false)
{
}

string JsonResponse::getBody()
{
	if (m_humanReadable) {
		Json::StyledWriter writer;
		return writer.write(*this);
	} else {
		Json::FastWriter writer;
		return writer.write(*this);
	}
}

void JsonResponse::setHuman(bool human)
{
	m_humanReadable = human;
}
}
