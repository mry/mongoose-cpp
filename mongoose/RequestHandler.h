#ifndef _MONGOOSE_REQUEST_HANDLER_H
#define _MONGOOSE_REQUEST_HANDLER_H

#include "Request.h"
#include "Response.h"
#include <string>

namespace Mongoose
{
class RequestHandlerBase {
public:
	virtual Response *process(Request &request) = 0;
	virtual ~RequestHandlerBase() = default;
};

template <typename T, typename R> class RequestHandler : public RequestHandlerBase {
public:
	typedef void (T::*fPtr)(Request &request, R &response);

	RequestHandler(T *controller_, fPtr function_)
		: m_controller(controller_)
		, m_function(function_)
	{
	}

	Response *process(Request &request)
	{
		R *response = new R;

		try {
			m_controller->preProcess(request, *response);
			(m_controller->*m_function)(request, *response);
		} catch (std::exception &ex) {
			return m_controller->serverInternalError(ex.what());
		} catch (...) {
			return m_controller->serverInternalError("Unknown error");
		}

		return response;
	}

protected:
	T *m_controller;
	fPtr m_function;
};
}

#endif
