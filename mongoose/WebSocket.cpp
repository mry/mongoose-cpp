#include <iostream>
#include <sstream>
#include "WebSocket.h"
#include "WebSockets.h"

using namespace std;

namespace Mongoose
{
WebSocket::WebSocket(struct mg_connection *connection_)
	: m_request(connection_)
	, m_connection(connection_)
{
}

void WebSocket::setId(int id_)
{
	m_id = id_;
}

int WebSocket::getId()
{
	return m_id;
}

void WebSocket::appendData(string data_)
{
	m_data += data_;
}

string WebSocket::flushData()
{
	string oldData = "";
	m_data.swap(oldData);

	return oldData;
}

Request &WebSocket::getRequest()
{
	return m_request;
}

void WebSocket::send(string data, int opcode)
{
	std::lock_guard<std::mutex> guard(m_mutex);
	if (isClosed()) {
		return;
	}

	try {
		if (!mg_websocket_write(m_connection, opcode, data.c_str(), data.size())) {
			m_closed = true;
		}
	} catch (...) {
		printf("FAILED to send to connection %p\n", (void *)m_connection);
		m_closed = true;
	}
}

void WebSocket::notifyContainers()
{
	std::lock_guard<std::mutex> guard(m_mutex);
	for (auto it : m_containers) {
		(it)->remove(this);
	}
}

void WebSocket::close()
{
	m_closed = true;
}

bool WebSocket::isClosed()
{
	return m_closed;
}

void WebSocket::addContainer(WebSockets *websockets)
{
	std::lock_guard<std::mutex> guard(m_mutex);
	m_containers.push_back(websockets);
}

void WebSocket::removeContainer(WebSockets *websockets)
{
	std::lock_guard<std::mutex> guard(m_mutex);
	vector<WebSockets *>::iterator it;

	// for (vector<WebSockets *>::iterator it : m_containers) {
	for (it = m_containers.begin(); it != m_containers.end(); it++) {
		if (*it == websockets) {
			it = m_containers.erase(it);
			break;
		}
	}
}

struct mg_connection *WebSocket::getConnection()
{
	return m_connection;
}
};
