#include <fstream>
#include <sstream>
#include <iostream>
#include "UploadFile.h"

using namespace std;

namespace Mongoose
{
UploadFile::UploadFile(string filename_, string data_)
	: m_filename(filename_)
	, m_data(data_)
{
}

string UploadFile::getName()
{
	return m_filename;
}

string UploadFile::getData()
{
	return m_data;
}

void UploadFile::saveTo(string directory)
{
	fstream file;
	file.open(directory + "/" + m_filename, fstream::out);
	file << m_data;
	file.close();
}

}
