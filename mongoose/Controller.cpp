#include <iostream>
#include "Controller.h"
#include "StreamResponse.h"

using namespace std;

namespace Mongoose
{
Controller::Controller()
	: m_sessions{ nullptr }
	, m_server{ nullptr }
	, m_prefix{ "" }
{
}

void Controller::setup()
{
}

void Controller::setServer(Server* server) 
{
    m_server = server;
}

void Controller::webSocketReady(WebSocket * /*websocket*/)
{
}

void Controller::webSocketData(WebSocket * /*websocket*/, string /*data*/)
{
}

Controller::~Controller()
{
	for (auto& route : m_routes) {
		delete route.second;
	}
	m_routes.clear();
}

bool Controller::handles(string method, string url)
{
	auto key = method + ":" + url;
	return (m_routes.find(key) != m_routes.end());
}

Response *Controller::process(Request &request)
{
	Response *response{ nullptr };

#ifdef ENABLE_REGEX_URL
	for (auto& [key, val] : m_routes) {
		if (request.match(key)) {
			response = val->process(request);
			break;
		}
	}
#else
	string key = request.getMethod() + ":" + request.getUrl();
	if (m_routes.find(key) != m_routes.end()) {
		response = m_routes[key]->process(request);
	}
#endif

	return response;
}

void Controller::preProcess(Request & /*request*/, Response & /*response*/)
{
}

void Controller::postProcess(Request & /*request*/, Response & /*response*/)
{
}

Response *Controller::handleRequest(Request &request)
{
	Response *response = process(request);

	if (response != nullptr) {
		postProcess(request, *response);
	}

	return response;
}

void Controller::setPrefix(string prefix_)
{
	m_prefix = prefix_;
}

void Controller::registerRoute(string httpMethod, string route, RequestHandlerBase *handler)
{
	string key = httpMethod + ":" + m_prefix + route;
	m_routes[key] = handler;
	m_urls.push_back(m_prefix + route);
}

void Controller::dumpRoutes()
{
	for (auto it : m_routes) {
		cout << (it).first << endl;
	}
}

Response *Controller::serverInternalError(string message)
{
	StreamResponse *response = new StreamResponse;

	response->setCode(HTTP_SERVER_ERROR);
	*response << "[500] Server internal error: " << message;

	return response;
}

vector<string> Controller::getUrls()
{
	return m_urls;
}

Session &Controller::getSession(Request &request, Response &response)
{
	return m_sessions->get(request, response);
}

void Controller::setSessions(Sessions *sessions_)
{
	m_sessions = sessions_;
}
}
