#include <iostream>
#include "WebSockets.h"

using namespace std;

namespace Mongoose
{
WebSockets::WebSockets(bool responsible_)
	: m_responsible(responsible_)
{
}

WebSockets::~WebSockets()
{
	if (m_responsible) {
		vector<WebSocket *> toDelete;

		for (auto it : m_websockets) {
			toDelete.push_back((it).second);
		}

		for (auto vit : toDelete) {
			remove(vit);
		}
	}
}

void WebSockets::add(WebSocket *websocket)
{
	if (websocket == nullptr) {
		return;
	}

	if (m_responsible) {
		int newId = 0;
		{
			std::lock_guard<std::mutex> guard(mutex);
			newId = m_id++;
		}
		websocket->setId(newId);
	}

	struct mg_connection *connection = websocket->getConnection();

	std::lock_guard<std::mutex> guard(m_mutex);
	if (m_websockets.find(connection) != m_websockets.end()) {
		remove(m_websockets[connection], false);
	}

	m_websocketsById[websocket->getId()] = websocket;
	m_websockets[connection] = websocket;
}

WebSocket *WebSockets::getWebSocket(int id)
{
	if (m_websocketsById.find(id) != m_websocketsById.end()) {
		return m_websocketsById[id];
	}

	return nullptr;
}

void WebSockets::sendAll(string data)
{
	{
		std::lock_guard<std::mutex> guard(m_mutex);
		for (auto it : m_websockets) {
			WebSocket *websocket = (it).second;
			websocket->send(data);
		}
	}
	clean();
}

void WebSockets::remove(WebSocket *websocket, bool lock)
{
	struct mg_connection *connection = websocket->getConnection();

	if (lock) {
		m_mutex.lock();
	}
	if (m_websockets.find(connection) != m_websockets.end()) {
		websocket->removeContainer(this);
		m_websockets.erase(connection);
		m_websocketsById.erase(websocket->getId());

		if (m_responsible) {
			websocket->close();
			websocket->notifyContainers();
			delete websocket;
		}
	}
	if (lock) {
		m_mutex.unlock();
	}
}

void WebSockets::remove(struct mg_connection *connection)
{
	std::lock_guard<std::mutex> guard(m_mutex);
	WebSocket *websocket = getWebSocket(connection);
	if (websocket) {
		websocket->removeContainer(this);
		m_websockets.erase(connection);
		m_websocketsById.erase(websocket->getId());

		if (m_responsible) {
			websocket->close();
			websocket->notifyContainers();
			delete websocket;
		}
	}
}

WebSocket *WebSockets::getWebSocket(struct mg_connection *connection)
{
	if (m_websockets.find(connection) != m_websockets.end()) {
		return m_websockets[connection];
	}

	return nullptr;
}

void WebSockets::clean()
{
	vector<WebSocket *> toDelete;

	std::lock_guard<std::mutex> guard(m_mutex);
	for (auto it : m_websockets) {
		if ((it).second->isClosed()) {
			toDelete.push_back((it).second);
		}
	}

	for (auto vit : toDelete) {
		remove(vit, false);
	}
}
};
